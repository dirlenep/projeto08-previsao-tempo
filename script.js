var btnBuscar = document.getElementById("idBtnBuscar");
var resultado = document.getElementById("idResultado");

resultado.style = "display: none";


btnBuscar.onclick = function previsao(){
    var local = document.getElementById("idInputBusca").value;

    fetch("https://weatherapi-com.p.rapidapi.com/forecast.json?q=" + local + "&days=3&lang=pt", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
            "x-rapidapi-key": "591d9c43e1msh92def5cfaf025a2p1eec64jsndaa0fedca8a9"
        }
        })
        .then(response => {
            return response.json();
        })
        .then(dados => {
            resultado.style = "display: block";

            document.getElementById("idCidade").innerHTML = "Cidade: " + local;

            document.getElementById("idDia1").innerHTML = "Previsão para hoje: "
            document.getElementById("idMin1").innerHTML = "Temperatura Mínima: " + dados.forecast.forecastday[0].day.mintemp_c;
            document.getElementById("idMax1").innerHTML = "Temperatura Máxima: " + dados.forecast.forecastday[0].day.maxtemp_c;
            document.getElementById("idTextoCondicao1").innerHTML = "Clima: " + dados.forecast.forecastday[0].day.condition.text;
            document.getElementById("idIconeCondicao1").innerHTML = "<img src=http:" + dados.forecast.forecastday[0].day.condition.icon + ">"

            document.getElementById("idDia2").innerHTML = "Previsão para amanhã: "
            document.getElementById("idMin2").innerHTML = "Temperatura Mínima: " + dados.forecast.forecastday[1].day.mintemp_c;
            document.getElementById("idMax2").innerHTML = "Temperatura Máxima: " + dados.forecast.forecastday[1].day.maxtemp_c;
            document.getElementById("idTextoCondicao2").innerHTML = "Clima: " + dados.forecast.forecastday[1].day.condition.text;
            document.getElementById("idIconeCondicao2").innerHTML = "<img src=http:" + dados.forecast.forecastday[1].day.condition.icon + ">"

            document.getElementById("idDia3").innerHTML = "Previsão para depois de amanhã: "
            document.getElementById("idMin3").innerHTML = "Temperatura Mínima: " + dados.forecast.forecastday[2].day.mintemp_c;
            document.getElementById("idMax3").innerHTML = "Temperatura Máxima: " + dados.forecast.forecastday[2].day.maxtemp_c;
            document.getElementById("idTextoCondicao3").innerHTML = "Clima: " + dados.forecast.forecastday[2].day.condition.text;
            document.getElementById("idIconeCondicao3").innerHTML = "<img src=http:" + dados.forecast.forecastday[2].day.condition.icon + ">"

            document.getElementById("idInputBusca").value = "";
        })
        .catch(err => {
            console.error(err);
            document.getElementById("idInputBusca").value = "";
            resultado.style = "display: none";
            document.getElementById("idErro").innerHTML = "Cidade não encontrada, tente novamente!"
    });

}
