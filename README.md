# Previsão do Tempo

Utilizando uma API para mostrar a previsão do tempo


## Links

- [Weather API](https://rapidapi.com/weatherapi/api/weatherapi-com/)


## Screenshots

- Scan do SonarQube
- [Scan](img/scanner.PNG)


## Linguagens Utilizadas

- JavaScript
- HTML
- Materialize